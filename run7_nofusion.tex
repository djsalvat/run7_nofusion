\documentclass[11pt,letterpaper]{article}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[margin=2.54cm]{geometry}
\usepackage[nodayofweek]{datetime}
\usepackage[toc,page]{appendix}
\usepackage[font={small,it}]{caption}
\usepackage{titlesec}
\newcommand{\sectionbreak}{\clearpage}

\def\name{Daniel J. Salvat}
\def\thetitle{R7 No-Fusion Analysis}

\hypersetup{
  colorlinks = true,
  urlcolor = blue,
  pdfauthor = {\name},
  pdfkeywords = {},
  pdftitle = {\thetitle},
  pdfsubject = {},
  pdfpagemode = UseNone
}

\renewcommand{\familydefault}{\sfdefault}

\begin{document}

\title{\thetitle}
\author{\name}

\maketitle

\section{Introduction\label{section_introduction}}

The purpose of the ``No-Fusion'' analysis is to produce decay time histograms which are free (or as free as possible) from the effects of fusion interference,
in order to facilitate the study of other systematic effects, such as electron interference, wall stops, and diffusion.
The original studies of the cuts using R4 MC are detailed in ELOG analysisnote:20.

The lifetime histograms with the $E_{9}$ cut should be highly effective for studies of effects within the fiducial volume. According to the MC studies,
this should reject $p-t$ fusions with better than $0.1$\% inefficiency. Outside of the fiducial volume, more stringent cuts must be used;
unfortunately, these cuts could suffer from a dependence upon the fusion time which is of order a few parts in 1000, which puts the residual
effects of these cuts on a similar footing to the effect of inadvertent fusion acceptance or deletion in the fiducial volume studied by MM.
The strict cuts, which were intended to aid in the interpretation of the lifetime fits near the walls of the TPC, thus need additional
modeling or study: one can attempt to model and fit the residual time dependence away, but fitting such a time dependence of the data in other contexts
has proven difficult (see e.g. ELOG analysis-run4:975). Beyond this, we must either
1) find a way to refine the cuts and make them more efficient; in general this could always be done with more strict energy cuts,
   though this is questionable as it could inadvertently lead to electron-interference effects that would not otherwise be present;
2) or, find a data-driven way to assess the efficiency and time dependence of the cut, so as to reduce the uncertainty in any kinetic fitting method
   which would be used to fit the rate near the walls.

The issues with the stricter cuts for wall-stops notwithstanding, we can at least use the established $E_{9}$ cut and assess their behavior and effectiveness on the R7 data.
It is an empirical question whether the goodness-of-fit and stability of parameter estimates is reasonable for these distributions.
If well-behaved, we can at least proceed to study effects which leverage the fiducial volume cut, such as impact parameter studies and electron interference.

In this report, we will address the following questions related to the $E_{9}$ cut histograms and the fits incorporating this $p-t$ rejection:
\begin{enumerate}
        \item Is the fitted disappearance rate stable within the fiducial volume?
        \item Is the goodness-of-fit reasonable throughout the fiducial volume?
        \item If the above criteria are satisfied, what is the stability of the fits versus gondola number?
        \item If the above criteria are satisfied, what is the stability of the fits versus impact parameter?
\end{enumerate}

\section{Data and data selection}

In this report, we analyze the summer 2016 MTA pass performed by RR, for which the relevant analysis modules were turned on.
The \texttt{NoFusionAnalysis} module simply makes the various fusion cuts, and fills \texttt{TLifetimeHistograms} objects
for clean block, HBE, lone-muon-stop events. In particular, we fill histograms subject to the additional requirement that $E_{9}<3000$ keV.
Fig. \ref{e9_cut} shows distributions of $E_{9}$ along with the chosen cut for all R7 data.

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.8\linewidth]{./figures/nofusion_E9_report.png}
        \caption{$E_{9}$ for the \texttt{StopThreshold} tracker for the fiducial volume and ROI. The vertical line shows the value of the cut used for the analysis.}
        \label{e9_cut}
\end{figure}

This cut was motivated by the studies of R4 MC, and is hopefully even more efficient for R7 due to the improvement in resolution;
however, there is the possibility of $n-^{3}$He fusion followed by $n-d$ scattering depositing considerable energy near the stop pad,
and the sensitivity of this cut to that effect has not yet been explored.
It should be noted that, with a fully-developed lifetime event tree from DJP, this analysis can be performed more systematically, and the direct impact of effects
such as $n-d$ recoil can be tested by scanning the value of the $E_{9}$ cut.

Unless otherwise noted, we will examine data using the \texttt{StopThresh} tracker.

\section{Kinetic fitting}

The data are fit to the fit function \texttt{TLifetimeFitMuMinusNoPT} which fits the full kinetics, but sets the $p-t$ recycling term (proportional to $1-\beta$) to zero.
The kinetic parameters are fixed, and no additional free parameters are introduced beyond the usual $N$,$R$, and $B$. This function is available in \texttt{LtFit}
and is committed to the CVS repository.

At this point we address what turns out to be an important detail: the number of non-fusion events are, of course, determined by the values of the fixed kinetic parameters.
While we fit the overall height (viz. $N$), the number of $n-^{3}$He fusions (and by our choice of $\beta$, the fraction of all fusions) is a prediction of this fit model.
In the course of performing fits to the R7 pass, the fits showed a residual effect indicative of fusion interference throughout the TPC,
always with a deficit of events at early times. A possible explanation is that the fit model \textit{underestimating} the number of $n-^{3}$He fusion events remaining in the data.
This effect can be remedied by increasing the chosen value of $\beta$ by about $1.4-\sigma$ of its experimental uncertainty of $0.015$, from $0.517$ to $0.517+0.022$.
This is shown for an example $z$-slice in fig. \ref{omega_residuals}.
In this report, we make this choice of $\beta$, as it appears to improve consistency. It should be noted, however, that there are presumably other
choices of kinetic parameters that could remedy this effect in the same way, such as decreasing the hyperfine transition rate, or decreasing the sticking fraction.
The data cannot distinguish between these choices, as they all lead to similar effects on the data.

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.4\linewidth]{./figures/tpc_residual_omega_nominal.png}
        \includegraphics[width=0.4\linewidth]{./figures/tpc_residual_omega_shifted.png}
        \caption{Left: the residuals for a fit to the TPC fiducial volume slice $z=4$ with the nominal $\omega$ value.
                 Right: the same, but with $\omega$ increased by $\sim1.4$ $\sigma$.}
        \label{omega_residuals}
\end{figure}

Finally, this raises the question of whether or not it is justified to make choices of fixed kinetic parameters, and whether a global analysis must be performed.
For the more general kinetic fitting of normal or fusion-selected data, the answer to this question is perhaps ; for the no-fusion data discussed here,
we are most sensitive to the time behavior of the recycling term, with $\beta$ exhibiting the largest uncertainty. Therefore, we fix it to the data for these studies.

\section{TPC scans}

\subsection{E9 cut}

All of the fits here are in a time window of 160 to 24000 ns. We simply use histview to perform the scans, which gives the fit parameters and $\chi^{2}$ of the fit.
Some examples of fit residuals will also be presented. Fig. \ref{nofusion_z_scan} shows the results of fitting with the NoPT fit function to the $z$ slices.
The red line in the upper right panel (i.e. the fitted disappearance rate) is a fit of the fiducial volume slices to a constant.
This gives a reduced $\chi^{2}$ consistent with $1$ in all cases.
The goodness-of-fit for the individual slices appears to be reasonable,
and the residuals do not appear to show any trend. The fit and fit residuals for $z=3$ are shown in fig. \ref{nofusion_zscan_residual}.
The $x$ and $y$ scans are presented in the same way in figs. \ref{nofusion_x_scan} and \ref{nofusion_y_scan}.

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.8\linewidth]{./figures/nofusion_e9_zscan.png}
        \caption{The $z$ scan fit results of the R7 data using the lifetime histograms with the $E_{9}$ cut, fit with the \texttt{TLifetimeFitMuMinusNoPT} fit function.
                 The fit start time is $160$ ns. The red line is a fit of a constant to the disappearance rate within the fiducial volume, in order to asses
                 to what extent the rate is constant for each slice. The fitted rate in the most downstream $z$ slice is off scale, several hundred $s^{-1}$ lower.}
        \label{nofusion_z_scan}
\end{figure}

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.8\linewidth]{./figures/nofusion_e9_zslice_residual_3.png}
        \caption{The fit and fit residuals for $z=3$. The other fiducial volume slices behave in the same way.}
        \label{nofusion_zscan_residual}
\end{figure}

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.8\linewidth]{./figures/nofusion_e9_xscan.png}
        \caption{The $x$ scan fit results of the R7 data using the lifetime histograms with the $E_{9}$ cut, fit with the \texttt{TLifetimeFitMuMinusNoPT} fit function.
                 The fit start time is $160$ ns. The red line is a fit of a constant to the disappearance rate within the fiducial volume, in order to asses
                 to what extent the rate is constant for each slice. The fitted rate in the $x$ slices outside the fiducial volume are off scale.}
        \label{nofusion_x_scan}
\end{figure}

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.8\linewidth]{./figures/nofusion_e9_yscan.png}
        \caption{The $y$ scan fit results of the R7 data using the lifetime histograms with the $E_{9}$ cut, fit with the \texttt{TLifetimeFitMuMinusNoPT} fit function.
                 The fit start time is $160$ ns. The red line is a fit of a constant to the disappearance rate within the fiducial volume, in order to asses
                 to what extent the rate is constant for each slice. The fitted rate in the $y$ slices outside the fiducial volume are off scale.}
        \label{nofusion_y_scan}
\end{figure}

\subsection{TPC scan for ``strict'' fusion cuts}

Finally, we consider the more ``strict'' no-fusion histograms that use the S-energy and ``fusion energy'' cuts. As discussed above, these cuts
can potentially be effective at rejecting fusion near the walls, but at the cost of some potential residual time dependence,
uncertainty in efficiency at the $0.1$\% to $1$\% level, induced electron-interference effects, or a combination of all three.
We attempt to fit these data using a kinetic fit function that assumes an error-function time dependence at early times,
and drops to zero after $\sim2$ $\mu$s, with a height determined by a free parameter $N_{fus}$. This fit function is motivated by the studies
of these cuts on MC data. A $z$-scan is shown in fig. \ref{senergy_fusionenergy_zscan}, and a residual display for $z=4$ is shown in fig. \ref{senergy_fusionenergy_residuals}.

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.8\linewidth]{./figures/nofusion_senergy_fusionenergy_zscan.png}
        \caption{The fit results for the S-energy and fusion-energy cut versions of the lifetime histograms, with a fit function incorporating a free parameter $N_{fus}$.
                 The fitted disappearance rate is highly discrepant, and the goodness-of-fit substantially worse than that for the $E_{9}$ cut.}
        \label{senergy_fusionenergy_zscan}
\end{figure}

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.8\linewidth]{./figures/nofusion_senergy_fusionenergy_zscan_residuals_4.png}
        \caption{The fit residuals for $z=4$ for the strict no-fusion cuts. Note the complex time dependence at early times.}
        \label{senergy_fusionenergy_residuals}
\end{figure}

\subsection{Discussion}

For the $E_{9}$ cut histograms, the qualitative feature from fusion interference (particularly in the $z$ scan) appears to be eliminated, or at least highly suppressed.
For comparison, fig. \ref{normal_z_scan} shows a $z$ scan with the usual exponential-plus-background fit, over the same range of time. The upward trend in disappearance rate versus $z$ is clear.
Moreover, the fitted disappearance rate over the fiducial volume is $\sim100$ s$^{-1}$ lower than for the fusionless data.

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.8\linewidth]{./figures/normal_zscan.png}
        \caption{A $z$ scan with an exponential-plus-background fit, with a $160$ ns start time. This shows the prominent fusion interference effect in the non-fusion-rejected data.}
        \label{normal_z_scan}
\end{figure}

The fusionless fit scans appear to be consistent with no remaining fusion interference effect,
and the individual fits themselves appear to describe the data well. As noted above, this agreement is only achieved with the minor tweak in the $p$-$t$
branching fraction. Without this tweak, the data appear to have more fusion events than the model. The (blinded) disappearance rate appears to differ
from that using an exponential fit; it's unclear whether this is a fit model deficiency, or there is a good reason for this.
Perhaps from a practical standpoint this isn't so important, since this analysis method would be used to explore the \textit{shift} in the lifetime
from other systematic effects.

The strict no-fusion cuts, however, appear to be problematic, and it is unclear how to proceed with their evaluation without re-evaluating the cuts on R7 MC,
finding a data-driven way to assess their efficacy, and devising a more sophisticated way to fit the data, if possible.
However, given the stability of the $E_{9}$ cuts, it is perhaps worth attempting fit scans vs $y$ to compare with EM's neutron yield analysis
to assess the lifetime shift from wall stops.

\section{Gondola scans}

Here we perform a similar study, looking at gondola number. We use \texttt{hLifetimeTPCFidVol*} histograms, looking at the normal and E9 cut versions.
The fits are over the $160$ to $24000$ ns range. We do not expect the gondola cut to cause any fusion-related effect;
however, the severity of the fiducial volume fusion interference is unclear,
and the use of the no-fusion analysis at least separates this effect from gondola related effects, like electron interference.
A gondola scan of all R7 data with an exponential fit is shown in the left side of fig. \ref{gondola_scan_exponential}.
A scan of only datasets with the correct collimator installation are shown in the right hand side.
Neither scan suggest any top/bottom discrepancy that one might expect from electron interference,
though the scan over all data appear to show some non-statistical fluctuation. This fluctiation is reduced by fitting at $1000$ ns,
and is potentially background-related. Background-related effects will be prevalent in \ref{section_impact_scan} as well.
The same scans, but with the NoPT fit function and E9-cut data, are shown in fig. \ref{gondola_scan_nofus}.
The qualitative behavior is very much the same.

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.47\linewidth]{./figures/histview_out_gondola_scan_exponential.png}
        \includegraphics[width=0.47\linewidth]{./figures/histview_out_gondola_scan_exponential_goodcoll.png}
        \caption{Left 4 panels: the scan for all R7 data. The fits for all gondolas and for the top and bottom halves are shown.
                 While there does not appear to be any evidence of ``gondola effect'', the fluctuations do not appear to be statistical.
                 The right panel is the same scan, but for all datasets with the beamline collimator correctly installed.
                 The fit is to a constant, showing basic consistency.}
        \label{gondola_scan_exponential}
\end{figure}

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.47\linewidth]{./figures/histview_out_gondola_scan_nofus.png}
        \includegraphics[width=0.47\linewidth]{./figures/histview_out_gondola_scan_nofus_goodcoll.png}
        \caption{The same scans as \ref{gondola_scan_exponential}, but for the fusionless data. The fit range is the same.
                 The qualitative behavior of the scans is very much like that of the full data.}
        \label{gondola_scan_nofus}
\end{figure}

This suggests that electron-interference effects are not appreciable at the $\sim12$ s$^{-1}$ level, though the data with the misaligned collimator data could be problematic.

\section{Impact Parameter Scans}\label{section_impact_scan}

A series of impact parameter scans are performed in ELOG analysisrun7:492, which tests the fit start and stop times, normal and fusionless data, ``good'' collimator data,
and examines scans with ``zero impact parameter'' (i.e. with impact parameter relative to the TPC center, not relative to the muon stop).
The various data selections and scans separate out a few different effects, which we discuss here.
The scans presented are for annuli of impact parameter, and not for the cumulative number of events below a given impact parameter cut.

\begin{figure}[ht!]
        \centering
        \includegraphics[width=0.7\linewidth]{./figures/impact_scan_compare.png} \\
        \includegraphics[width=0.7\linewidth]{./figures/impact_scan_compare_zoom.png}
        \caption{Scans over impact parameter for different fit windows, datasets, and data types.}
        \label{impact_scan_compare}
\end{figure}

The three scans over all data (black, blue, and cyan) all show a drastic increase in rate with increasing impact parameter.
As discussed in the ELOG post, changing the fit \textit{stop} time to $12$ $\mu$s somewhat reduces the size of this effect, but does not eliminate it.
Those same data also exhibit the kicker ``step'' in the residuals for slices of large impact parameter.

The lower panel of fig. \ref{impact_scan_compare} zooms in to fits at low impact parameter, and only shows the subset of runs with the fixed collimator.
Two features are evident: 1) the use of the no-fusion version of the histograms suppresses much of the dependence on impact parameter for low values,
and 2) using the ``zero'' impact parameter further reduces the variation. In fact, this latter case is consistent with a constant rate versus
impact parameter with $\chi^{2}/DOF = 33.66/29$. This suggests that the no-fusion histograms remove the effect of fusion interference,
while being subject to the effect of diffusion, which would be expected to increase the rate for small impact parameter.
The ``zero'' impact parameter does not depend upon the stop distribution, and should be insensitive to diffusion,
which, at the level of statistics for these data, appears to be the case.

\section{Discussion}

The no-fusion histograms appear to produce stable fits within the fiducial volume of the TPC, with reasonable goodness-of-fit,
as long as a minor adjustment to the kinetic parameters is made. No non-statistical fluctiation in the fitted rate are observed.
The scans over gondola number do not show a top/bottom discrepancy indicative of electron interference, though the inclusion of data with the misaligned collimator
appears to cause some non-statistical fluctuation in the fits. For a $1000$ ns start time, with only the good collimator data,
if we assume that the ``top'' subset of the data is essentially free of the effect, while the ``bottom'' suffers from the effect,
this bounds the effect at the $\sim15$ s$^{-1}$ level.

The effect of the misaligned collimator is further evident in the impact parameter scans, causing a considerable effect for data with large impact parameter.
The data also show variation at low impact parameter, which is greatly suppressed, but not eliminated by the use of the fusionless data.
The remaining effect at small impact parameter is ostensibly due to diffusion, as suggested by the analysis of the ``zero'' impact parameter data.
The fusionless impact parameter scans could be fitted with S. Clayton's prescription for diffusion to place a bound on this effect.

While consistency is achieved in some cases, two main issues are:
\begin{itemize}
        \item The fusionless data seem to suggest some amount of diffusion. This would have to be quantified for a nominal cut of $\sim120$ mm,
              or we would have to consider whether the ``zero'' impact parameter is the better cut to make.
        \item More importantly, the data with the misaligned collimator are generally problematic. Regardless of the choice of impact parameter cut,
              there appears to be an effect which is not entirely eliminated by fitting past the kicker step at $1$ $\mu$s.
              The dependence upon gondola number is effected by this as well, perhaps due to the sizeable variation in background for each gondola section
              (see for example the left panel of fig. \ref{gondola_scan_nofus}).
\end{itemize}

\end{document}
